Rails.application.routes.draw do

  resources :orders

  mount Payanyway::Engine => '/payanyway'
  devise_for :users, controllers: { registrations: "registrations", sessions: 'sessions' }
  resources :users do
    member do
      get 'buy_turnir'
      get 'new_order'
    end
  end

  resources :jsonparsers

  resources :winpacks do
    member do
      get 'archurl'
    end
  end

  resources :turnirs
  match '/result_turnir',  to: 'turnirs#result', via: 'post'
  match '/show_turnir',  to: 'turnirs#show_number', via: 'post'

  resources :jsonparsers

  get 'pages/index'

  resources :verdicts

  get '/logs', to: 'pages#logs'
  get '/forgot', to: 'pages#forgot'
  get '/deletetoken/:id', to: 'pages#deletetoken'

  get '/edit/:turnir/:token', to: 'pages#editanswers'


  #get '/deleteall', to: 'pages#deleteall'
  resources :answers

  resources :members

  resources :teams

  resources :results
  get '/results2', to: 'results#index2'

  get '/api/auth', to: 'api#login'
  get '/api/auth/exit', to: 'api#exit'
  post '/api/result', to: 'api#result'
  post '/api/request', to: 'api#requestin'
  get '/faq', to: 'pages#faq'
  get '/api/turnirs', to: 'api#turnirs'
  get '/contacts', to: 'pages#contacts'
  get '/oferta', to: 'pages#oferta'
  get '/newclubsoffer', to: 'pages#newclubsoffer'
  get '/privacy', to: 'pages#privacy'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'pages#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end