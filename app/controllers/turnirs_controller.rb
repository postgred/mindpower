class TurnirsController < ApplicationController
  before_action :set_turnir, only: [:show, :edit, :update, :destroy]
  before_action :admin_user!, only: [:new, :create, :edit, :update, :destroy]

  # GET /turnirs
  # GET /turnirs.json
  def index
    @turnirs = Turnir.all
    @pageid = 'result'
  end

  # GET /turnirs/1
  # GET /turnirs/1.json
  def show
    @pageid = 'result'
    @answers = Answer.where(:turnir_id => @turnir.numbertur)
    @tks = (Team.where(:turnir_id => @turnir.numbertur) + Team.where(:token => @answers.collect(&:token))).collect(&:token)
    @tit = @answers.collect(&:number).max
    @teams = Team.where(:token => @tks)
  end

  # GET /turnirs/new
  def new
    @turnir = Turnir.new
  end

  # GET /turnirs/1/edit
  def edit
  end

  # POST /turnirs
  # POST /turnirs.json
  def create
    @turnir = Turnir.new(turnir_params)

    respond_to do |format|
      if @turnir.save
        format.html { redirect_to '/turnirs', notice: 'Turnir was successfully created.' }
        format.json { render :show, status: :created, location: @turnir }
      else
        format.html { render :new }
        format.json { render json: @turnir.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /turnirs/1
  # PATCH/PUT /turnirs/1.json
  def update
    respond_to do |format|
      if @turnir.update(turnir_params)
        format.html { redirect_to  '/turnirs', notice: 'Turnir was successfully updated.' }
        format.json { render :show, status: :ok, location: @turnir }
      else
        format.html { render :edit }
        format.json { render json: @turnir.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /turnirs/1
  # DELETE /turnirs/1.json
  def destroy
    @turnir.destroy
    respond_to do |format|
      format.html { redirect_to turnirs_url, notice: 'Turnir was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def result
    @turnir = Turnir.find(params[:turnir][:id])
    @answers = Answer.where(:turnir_id => @turnir.numbertur)

    @answers.each do |answer|
      answer.real_time = answer.real_time
=begin
      if answer.right == false
        answer.resulttime = @turnir.wrongtime
      else
        if answer.real_time <= @turnir.downtime
          answer.resulttime = @turnir.downtime
        else
          answer.resulttime = answer.real_time
        end
      end
=end
      case answer.right
	when true
	  if answer.real_time <= @turnir.downtime
	    answer.resulttime = @turnir.downtime
	  else
	    answer.resulttime = answer.real_time
	  end
	when false
	  answer.resulttime = @turnir.wrongtime
	when nil
	  answer.resulttime = nil
      end
      answer.save
    end

    respond_to do |format|
      format.js{}
    end
  end

  def show_number

    @turnir = Turnir.find(params[:turnir][:id])
    @answers = Answer.where(:turnir_id => @turnir.numbertur)
    @tks = (Team.where(:turnir_id => @turnir.numbertur) + Team.where(:token => @answers.collect(&:token))).collect(&:token)
    @tit = @answers.collect(&:number).max
    @teams = Team.where(:token => @tks)

    if current_user != nil && current_user.is_admin       
      @turnir.show = params[:turnir][:show]
      @turnir.save
    end

    respond_to do |format|
      format.js {
                 render :action => 'show_number', 
                        :locals => { :show => params[:turnir][:show]} 
                }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_turnir
      @turnir = Turnir.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def turnir_params
      params.require(:turnir).permit(:name, :file_question, :start_game, :numbertur, :end_game, :description, :game_type, :downtime, :wrongtime, :price)
    end
end
