class VerdictsController < ApplicationController

  before_filter :controller

  def create
    @answers = Answer.where(:answer => params[:verdict][:answervalue], :number => params[:verdict][:question])
    @answers.each do |answer|
      answer.update_attributes(:right => params[:verdict][:verdict])
    end
     if params[:verdict][:verdict] == '0'
       @w = "Отклонен"
     elsif params[:verdict][:verdict] == '1'
       @w = "Принят"
     else 
       @w = "Недопустимый параметр"
     end
     render 'update', :locals => {:word => @w}

  end



  private
    def answerss_params
      params.require(:answer).permit(:answervalue, :verdict, :turnir, :question, :i)
    end
end
