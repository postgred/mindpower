class PagesController < ApplicationController
  before_filter :controller, only: [:deleteall, :deletetoken]
  def signup
    @pageid = 'reg2'
if current_user
@pageid = 'reg'
end
  end

  def faq
  @pageid = 'faq'
  end

  def deletetoken
   Answer.where(:token => params[:id]).each do |an|
      an.destroy
   end
   Team.where(:token => params[:id]).each do |te|
      te.destroy
   end
   Member.where(:token => params[:id]).each do |me|
    me.destroy
   end
   redirect_to '/results'
  end

  def signin
  end

 def deleteall
   Answer.all.each do |an|
     an.destroy
  end
  Team.all.each do |te|
    te.destroy
  end
  Member.all.each do |me|
    me.destroy
  end
  redirect_to '/users'
 end

  def logs
    @logs = Array.new
    Log.all.each do |log|
      @logs << log.created_at.to_s + ' -- ' + log.log.to_s
    end 
    send_data @logs.map(&:inspect).join(', ').gsub(',',"\n") , :filename => 'logs.txt'
  end

  def forgot
    if params[:surname] != nil && params[:mail] != nil
      @user = User.where(:surname => params[:surname], :mail => params[:mail]).first
        if @user
           @m = "Для вас сгенерирован новый пароль и отправлен на почту. Письмо придет в течение 10 минут"
           @key = SecureRandom.hex(3) 
           @user.update_attributes(:password => @key, :password_confirmation => @key )
           UserMailer.remember(@user, @key).deliver
        else
           @m = "Пользователь с таким E-mail и фамилией не найден"
        end
    end
  end 
end
