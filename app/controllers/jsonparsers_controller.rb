class JsonparsersController < ApplicationController
  before_action :set_jsonparser, only: [:show, :edit, :update, :destroy]

  # GET /jsonparsers
  # GET /jsonparsers.json
  def index
    @jsonparsers = Jsonparser.all
  end

  # GET /jsonparsers/1
  # GET /jsonparsers/1.json
  def show
  end

  # GET /jsonparsers/new
  def new
    @jsonparser = Jsonparser.new
  end

  # GET /jsonparsers/1/edit
  def edit
  end

  # POST /jsonparsers
  # POST /jsonparsers.json
  def create
  @jsonparser = Jsonparser.new(jsonparser_params)
   data = JSON.parse(@jsonparser.json_body)
 if data != nil
    # if data["version"] && data["version"] == '0.0.1' 

    user =User.find_by(mail: data["token"])
    if !Token.where(:user_id => user.id).empty?
      token = Token.where(:user_id => user.id).last
    else
      @key = SecureRandom.hex(50)
      Token.create!(:token => @key, :user_id => user.id)
      token = Token.where(:token => @key).last
    end 

    if token 
          Team.create!(:token => token.id, :team => user.organisation.to_s, :city => user.city.to_s, :turnir_id => '4') if Team.where(:token => token.id, :team => user.organisation.to_s, :city => user.city.to_s).empty?

          data["game_report"]["team"]["members"].each do |member|
             Member.create!(:token => token.id, :family => member["surname"].to_s, :name_fst => member["name"].to_s, :name_scnd => member["patrony"].to_s, :membership => member["membership"].to_s) if Member.where(:token => token.id, :family => member["surname"].to_s, :name_fst => member["name"].to_s, :name_scnd => member["patrony"].to_s).empty?
          end

  data["game_report"]["results"]["answers"].each do |data|
         if Answer.where(:number => data["number"].to_i, :turnir_id =>  '4', :token => token.id).empty?
           if Answer.where(:number => data["number"].to_i, :answer => data["answer"].to_s, :turnir_id =>  '4').empty?
             Answer.create!(:token => token.id, :number =>  data["number"].to_i, :turnir_id =>  '4', :answer => data["answer"].to_s)
           else
             @oldright = Answer.where(:number => data["number"].to_i, :answer =>  data["answer"].to_s, :turnir_id =>  '4').first
             Answer.create!(:token => token.id, :number => data["number"].to_i, :answer =>  data["answer"].to_s, :right => @oldright.right, :turnir_id =>  '4')
           end
           
         end
  end


         @ans =  Answer.where(:token => token.id, :turnir_id =>  '4')
         @arry = ''
         @ans.each do |ans|
           @arry << ans.number.to_s + '. ' + ans.answer.to_s + ' <br>'
         end
         render text: "Результаты игры: принято ответов на вопросы: " + @ans.count.to_s + "<br><br> " + @arry.to_s + "<br> Спасибо за игру! Ваши данные успешно приняты сервером игры, теперь можно закрыть программу."
         Log.create!(:log => "Запрос: " + data.to_s + " || Ответ: Результаты игры: принято ответов на вопросы: " + @ans.count.to_s + "<br><br> " + @arry.to_s + "<br> Спасибо за игру! Ваши данные успешно приняты сервером игры, теперь можно закрыть программу.")
     


    end
  else
     render text: '{"code":"UNSAVE"}'
  end
  end

  # PATCH/PUT /jsonparsers/1
  # PATCH/PUT /jsonparsers/1.json
  def update
    respond_to do |format|
      if @jsonparser.update(jsonparser_params)
        format.html { redirect_to @jsonparser, notice: 'Jsonparser was successfully updated.' }
        format.json { render :show, status: :ok, location: @jsonparser }
      else
        format.html { render :edit }
        format.json { render json: @jsonparser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jsonparsers/1
  # DELETE /jsonparsers/1.json
  def destroy
    @jsonparser.destroy
    respond_to do |format|
      format.html { redirect_to jsonparsers_url, notice: 'Jsonparser was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jsonparser
      @jsonparser = Jsonparser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jsonparser_params
      params.require(:jsonparser).permit(:json_body)
    end
end
