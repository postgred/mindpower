class SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    @u = User.where(:email => params[:user][:email]).first
    if @u.encrypted_password == ''
      flash[:notice] = 'По техническим причинам вы не сможете авторизоваться со старым паролем. Повторите ваш e-mail в форме ниже и мы вышлем вам ссылку на восстановление пароля'
      redirect_to new_user_password_path
    else
      super
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
