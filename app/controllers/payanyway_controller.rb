class PayanywayController < ApplicationController
  def success

    # вызывается при отправке шлюзом пользователя на Success URL.
    #
    # ВНИМАНИЕ: является незащищенным действием!
    # Для выполнения действий после успешной оплаты используйте pay_implementation

    #redirect_to root_path

    @order = Order.find(params[:MNT_TRANSACTION_ID])
    @order.status = "success"
    @order.save
    @order.user.check += @order.amount
    @order.user.save
    flash[:success] = "Пополнение кошелька прошло успешно"
    if current_user
      redirect_to user_path(current_user)
    else
      flash[:notice] = 'Оплата прошла успешно. Авторизируйтесь заного.'
      redirect_to new_user_session_path
    end
  end

  def pay
    # вызывается при оповещении магазина об 
    # успешной оплате пользователем заказа. (Pay URL)
    #
    # params[ KEY ], где KEY ∈ [ :moneta_id, :order_id, :operation_id,
    # :amount, :currency, :subscriber_id, :test_mode, :user, :corraccount,
    # :custom1, :custom2, :custom3 ]
    logger.info "PAY ПРИШЕЛ!!!!"
  end

  def fail
    # вызывается при отправке шлюзом пользователя на Fail URL.
  end
end