#encoding: utf-8
class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :admin_user!, only: [:index, :destroy]
  # GET /users
  # GET /users.json
  def index
    @users = User.all#User.where('verifed IS NOT ?', true)
  respond_to do |format|  ## Add this
    format.csv { render text: @users.to_csv}
    format.html 
    ## Other format
  end      

  end

  # GET /users/1
  # GET /users/1.json
  def show
    @turnirs = @user.bougth_turnirs
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end


  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
sign_in @user
        format.js {  render :js => "window.location.reload()" }
      else
        format.js { render 'create' }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to '/users', notice: 'User was successfully updated.' }
        format.js { render 'update' }
      else
        format.html { render :edit }
        format.js { render 'error' }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def new_order
    @order = current_user.orders.build(amount: params[:check], status: "new", order_type: "refill")
    @order.save
    url_params = {order_id: @order.id,
                  amount: @order.amount,
                  description: "#{current_user.name} #{current_user.surname} пополняет кошелек на сумму #{@order.amount}руб."
    }
    redirect_to Payanyway::Gateway.payment_url(url_params, use_signature = false)
  end

  def buy_turnir

    turnir = Turnir.find(params[:user][:turnir])

    if current_user.buy_turnir!(turnir)
      flash[:success] = "Покупка прошла успешно"
    else
      flash[:alert] = "Ошибка. У вас уже есть этот турнил, либо недостаточно денег для его покупки. Пополните свой кошелек в личном кабинете"
    end
    redirect_to turnirs_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :city, :organisation, :surname, :patronym, :is_admin, :is_gamer, :verifed, :mail, :password_digest, :password, :password_confirmation)
    end
end
