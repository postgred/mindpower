class WinpacksController < ApplicationController
  before_action :set_winpack, only: [:show, :edit, :update, :destroy, :archurl]
  #before_filter :controller
  # GET /winpacks
  # GET /winpacks.json
  def index
    @winpacks = Winpack.all
  end

  # GET /winpacks/1
  # GET /winpacks/1.json
  def show
  end

  # GET /winpacks/new
  def new
    @winpack = Winpack.new
  end

  # GET /winpacks/1/edit
  def edit
    #system("unzip #{w.file_pack.path} -d #{w.file_pack.path.gsub(w.file_pack_identifier, '')}")

  end

  # GET /winpacks/1/archurl
  def archurl
    dir = @winpack.file_pack.path.gsub(@winpack.file_pack_identifier, '')
    url = @winpack.file_pack.url
    (1..@winpack.file_pack_identifier.length).each { url.chop! }
    system("mkdir #{dir}archive")
    system("unzip -o #{@winpack.file_pack.path} -d #{dir}archive")
    respond_to do |format|
      format.js { render :action => 'archurl', :locals => {:url => url}}
    end
  end

  # POST /winpacks
  # POST /winpacks.json
  def create
    @winpack = Winpack.new(winpack_params)

    respond_to do |format|
      if @winpack.save
        format.html { redirect_to '/winpacks', notice: 'Winpack was successfully created.' }
        format.json { render :show, status: :created, location: @winpack }
      else
        format.html { render :new }
        format.json { render json: @winpack.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /winpacks/1
  # PATCH/PUT /winpacks/1.json
  def update
    respond_to do |format|
      if @winpack.update(winpack_params)
        format.html { redirect_to  '/winpacks', notice: 'Winpack was successfully updated.' }
        format.json { render :show, status: :ok, location: @winpack }
      else
        format.html { render :edit }
        format.json { render json: @winpack.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /winpacks/1
  # DELETE /winpacks/1.json
  def destroy
    @winpack.destroy
    respond_to do |format|
      format.html { redirect_to winpacks_url, notice: 'Winpack was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_winpack
      @winpack = Winpack.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def winpack_params
      params.require(:winpack).permit(:platform, :version, :minversion, :file_pack, :client_type, :json)
    end
end
