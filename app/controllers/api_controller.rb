class ApiController < ApplicationController

skip_before_filter  :verify_authenticity_token
before_filter :configure_charsets 

def login
  user = User.find_by(mail: params[:login].downcase)
  if user && user.authenticate(params[:password])
    if user
      @pack = Winpack.where('platform LIKE ?', "%#{params[:os]}%").last  
      @minverion = @pack.minversion
      if Token.where(:user_id => user.id, :exit => nil).last
	logger.info "Token: 1"
        @key = Token.where(:user_id => user.id).last.token
        token = Token.where(:user_id => user.id).last
	logger.info "#{token}"
	logger.info "#{@key}"
      else
	logger.info "Token: 2"
        @key = SecureRandom.hex(50)
        if Token.create!(:token => @key, :user_id => user.id)
	else
	end
      end
      if params[:version].to_s == '0'.to_s
        #params[:version].to_f < @minverion.to_f
          render(
             json: Jbuilder.encode do |j|
               j.code 'SHOW'
               j.popup 'Версия вашего клиента устарела. Пожалуйста, скачайте актуальную версию на mindpowergame.com'
             end,
             status: 200
           ).to_json.encode("cp1251")
           return
       else
          render(
             json: Jbuilder.encode do |j|
               j.code 'OK'
               j.access_token @key
	       j.role "player"
               j.city user.city
               j.popup 'test'

	         	  j.team do
 	          	  j.title 'Три веселых гуся и еще столько же'
	          	  j.members Member.where(:token => Token.where(:user_id => user.id).first.id).each do |member|
                             j.id member.id
                             j.surname member.family
                             j.name member.name_fst
			     j.patrony "Сидоров"
                             j.membership member.membership
                            end
                          
	          	end
            end,
             status: 200
           ).to_json.encode("cp1251")
           return
       end

    else 
     render(
        json: Jbuilder.encode do |j|
          j.code 'NOTACTIVE'
        end,
        status: 200
      )
      return
    end
  else
     render(
        json: Jbuilder.encode do |j|
          j.code 'NOTAUTH'
        end,
        status: 200
      )
      return
  end

end

def exit
  token =Token.find_by(token: params[:token])
  if token
    token.update_attribute(:exit, true)
    render(
        json: Jbuilder.encode do |j|
          j.code 'OK'
        end,
        status: 200
     )
     return
  else
     render(
        json: Jbuilder.encode do |j|
          j.code 'FAIL'
        end,
        status: 200
     )
     return
  end

end

def turnirs
  app = Winpack.where('json IS NOT NULL').last
  render text: app.json        
end

def result

  data = JSON.parse(request.body.read).encode("cp1251")
  
  if data != nil
  # if data["version"] && data["version"] == '0.0.1' 

    token =Token.find_by(token: data["token"])

    if token 
      Team.create!(
        :token => token.id, 
        :team => data["game_report"]["team"]["title"].to_s, 
        :city => data["game_report"]["team"]["city"].to_s, 
        :turnir_id => data["game_report"]["results"]["turnir_id"].to_i
      )

      data["game_report"]["team"]["members"].each do |member|
        Member.create!(
          :token => token.id, 
          :family => member["surname"].to_s, 
          :name_fst => member["name"].to_s, 
          :name_scnd => member["name_scnd"].to_s, 
          :membership => member["membership"].to_s
        )
      end

      data["game_report"]["results"]["answers"].each do |member|
        if Answer.where(
            :number => member["number"].to_i, 
            :answer => member["answer"].to_s
            ).empty?

           Answer.create!(
             :token => token.id, 
             :number => member["number"].to_i, 
             :answer => member["answer"].to_s
           )

         else

           @oldright = Answer.where(
             :number => member["number"].to_i, 
             :answer => member["answer"].to_s
           ).first

           Answer.create!(
             :token => token.id, 
             :number => member["number"].to_i, 
             :answer => member["answer"].to_s, 
             :right => @oldright.right
           )
         end
      end

      Log.create!( :log=>data["game_report"]["log"]["log"].to_s)

      render text: '{"code":"OK"}'
    else
      render text: '{"code":"UNKNOWNTOKEN"}'
    end
 # else 
   #  render(
   #     json: Jbuilder.encode do |j|
    #      j.code 'OLD'
    #    end,
   #     status: 200
   #   )
   #   return
 # end
    else
      render text: '{"code":"UNSAVE"}'
  end
end 

def requestin
  data = JSON.parse(request.body.read)
  
  if data != nil

    token =Token.find_by(token: data["token"])

    if token

  	  user = User.find(token.user_id)

      case data["request"]

        when "game_start"
          
          if Team.where(
              :token => token.id, 
              :team => data["team"]["title"].to_s, 
              :city => user.city.to_s
              ).empty?

            Team.create!(
              :token => token.id, 
              :team => data["team"]["title"].to_s, 
              :city => user.city.to_s, 
              :turnir_id => data["turnir_id"].to_i
            ) 
          end

          data["team"]["members"].each do |member|
            if Member.where(
                            :token => token.id, 
                            :family => member["surname"].to_s, 
                            :name_fst => member["name"].to_s, 
                            :name_scnd => member["patrony"].to_s
                           ).empty?

              Member.create!(
                :token => token.id, 
                :family => member["surname"].to_s, 
                :name_fst => member["name"].to_s, 
                :name_scnd => member["patrony"].to_s, 
                :membership => member["membership"].to_s)           
            end
          end
          render text: '{"code":"OK"}'

        when 'answer'

            if Answer.where(
                :number => data["number"].to_i, 
                :turnir_id =>  data["turnir_id"], 
                :token => token.id, 
                :real_time => data["time"].to_i
                ).empty?

                if Answer.where(
                  :number => data["number"].to_i, 
                  :answer => data["text"].to_s, 
                  :turnir_id =>  data["turnir_id"].to_i, 
                  :real_time => data["time"].to_i
                  ).empty?

                  Answer.create!(
                    :token => token.id, 
                    :number =>  data["number"].to_i, 
                    :turnir_id =>  data["turnir_id"], 
                    :answer => data["text"].to_s, 
                    :real_time => data["time"].to_i)

                else

                  @oldright = Answer.where(
                    :number => data["number"].to_i, 
                    :answer =>  data["text"].to_s, 
                    :turnir_id =>  data["turnir_id"], 
                    :real_time => data["time"].to_i
                    ).first

                  Answer.create!(
                    :token => token.id, 
                    :number => data["number"].to_i, 
                    :answer =>  data["text"].to_s, 
                    :right => @oldright.right, 
                    :turnir_id =>  data["turnir_id"], 
                    :real_time => data["time"].to_i)
                end
                render text: '{"code":"OK"}'
            end

        when 'gameoffline'
	        render text: '{"code":"OK"}'

        when 'gamestop'

          @ans =  Answer.where(:token => token.id, :turnir_id =>  data["turnir_id"].to_i)
          @arry = ''

          @ans.each do |ans|
            @arry << ans.number.to_s + '. ' + ans.answer.to_s + ' <br>'
          end

          render text: "Результаты игры: принято ответов на вопросы: " + 
                  @ans.count.to_s + "<br><br> " + @arry.to_s + 
                  "<br> Спасибо за игру! Ваши данные успешно приняты сервером игры, 
                  теперь можно закрыть программу."

          Log.create!(
            :log => "Запрос: " + data.to_s + " || Ответ: Результаты игры: 
              принято ответов на вопросы: " + @ans.count.to_s + "<br><br> " + @arry.to_s + 
              "<br> Спасибо за игру! Ваши данные успешно приняты сервером игры, 
              теперь можно закрыть программу."
          )

    	  when 'turnirs_list'
	    
	    	  @turnirs = Turnir.all
	    
          render(
      
            json: Jbuilder.encode do |j|

              j.time_now 	Time.now.strftime("%Y-%m-%dT%H:%M:%S")

              j.turnirs @turnirs do |turnir|  
                j.id turnir.numbertur
                j.status "accepted"
                j.type turnir.game_type
                j.title turnir.name
                j.begin turnir.start_game
                j.end turnir.end_game
                j.file_size turnir.file_question.size
                j.file_pass "qeJdsE"
                j.file_link "https://mindpowergame.com" + turnir.file_question.url
                j.file_name turnir.file_question_identifier
                j.max_number turnir.max_number
              end
            end,
            status: 200
        ).to_json.encode("cp1251")
      
        logger.info("turnir_list")
      end
    else

      case data["request"]

      	when "test_server"

          logger.info "test_server"
          render :json => {:code => "OK"}.to_json.encode("cp1251")
        
        when "current_version"
          app = Winpack.where('json IS NOT NULL').last
          render text: app.json        
      end

      #render text: '{"code":"UNKNOWNTOKEN"}'
    end
  else
     render text: '{"code":"UNSAVE"}'
  end
end 



def configure_charsets 
   headers["Content-Type"] = "text/json; charset=windows-1251"     
end 

end
