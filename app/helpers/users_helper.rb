module UsersHelper

  def admin_user!
    unless current_user.is_admin?
      redirect_to root_path
    end
  end
end
