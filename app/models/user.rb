#encoding:utf-8
class User < ActiveRecord::Base

  has_many :purchases, dependent: :destroy, foreign_key: "user_id"
  has_many :bougth_turnirs, through: :purchases, source: :turnir
  has_many :orders

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def self.to_csv
    (CSV.generate do |csv|
      csv << column_names
      all.each do |user|
        csv << user.attributes.values_at(*column_names)
      end
    end).encode('UTF-8', :undef => :replace, :replace => '')
  end

  def buy_turnir!(turnir)

    if check > turnir.price
      begin
        purchases.create!(turnir_id: turnir.id)
        self.check -= turnir.price
        self.save
        @order = self.orders.build(amount: turnir.price, status: "success", order_type: "turnir")
        @order.save

        logger.info "Done!"
      rescue
        logger.info "Вы уже купили этот турнир"
      end
    else
      logger.info "Не достаточно денег"
    end
  end
end
