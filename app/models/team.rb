class Team < ActiveRecord::Base


  def top(turnir, show, type, local_show)
    case type
    when "single"
      Answer.where(:token => self.token, :right => true, :turnir_id => 
                   turnir.to_i).where("number <= " + show.to_s).where("number <= " + local_show.to_s).map(&:number).uniq.count

    when "race"
      Answer.where(:token => self.token, :turnir_id =>
                   turnir.to_i).where("number <= " + show.to_s).where("number <= " + local_show.to_s).sum(:resulttime)
    end
  end

end
