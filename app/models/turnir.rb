class Turnir < ActiveRecord::Base
  has_many :purchases, foreign_key: "turnir_id"
  has_many :users, through: :purchases
  mount_uploader :file_question, FileUploader
end
