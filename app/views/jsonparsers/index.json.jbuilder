json.array!(@jsonparsers) do |jsonparser|
  json.extract! jsonparser, :id, :json_body
  json.url jsonparser_url(jsonparser, format: :json)
end
