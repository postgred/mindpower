json.array!(@turnirs) do |turnir|
  json.extract! turnir, :id, :name, :file_question, :start_game, :end_game, :description
  json.url turnir_url(turnir, format: :json)
end
