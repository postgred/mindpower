json.array!(@winpacks) do |winpack|
  json.extract! winpack, :id, :platform, :version, :minversion, :file_pack
  json.url winpack_url(winpack, format: :json)
end
