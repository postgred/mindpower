json.array!(@users) do |user|
  json.extract! user, :id, :name, :surname, :patronym, :is_admin, :verifed, :mail, :password_digest, :password, :password_confirmation
  json.url user_url(user, format: :json)
end
