#encoding: utf-8
class UserMailer < ActionMailer::Base
  default from: "robot@mindpowergame.com"
  def remember(user, key)
    @pass = key
    mail(to: user.mail, subject: 'Восстановление пароля')
  end  

end
