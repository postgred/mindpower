class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :surname
      t.string :patronym
      t.boolean :is_admin
      t.boolean :verifed
      t.string :mail
      t.string :password_digest
      t.string :password
      t.string :password_confirmation

      t.timestamps
    end
  end
end
