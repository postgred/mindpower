class AddRealimeToAnswer < ActiveRecord::Migration
  def change
    add_column :answers, :real_time, :integer
  end
end
