class AddTimesToTurnir < ActiveRecord::Migration
  def change
    add_column :turnirs, :downtime, :integer
    add_column :turnirs, :wrongtime, :integer
  end
end
