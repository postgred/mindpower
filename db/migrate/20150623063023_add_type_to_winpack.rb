class AddTypeToWinpack < ActiveRecord::Migration
  def change
    add_column :winpacks, :client_type, :string
  end
end
