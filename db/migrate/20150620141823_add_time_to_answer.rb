class AddTimeToAnswer < ActiveRecord::Migration
  def change
    add_column :answers, :time, :integer
  end
end
