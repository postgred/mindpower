class AddCheckToUser < ActiveRecord::Migration
  def change
    add_column :users, :check, :integer, default: 0
  end
end
