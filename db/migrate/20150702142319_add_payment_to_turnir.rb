class AddPaymentToTurnir < ActiveRecord::Migration
  def change
    add_column :turnirs, :price, :integer, default: 0
  end
end
