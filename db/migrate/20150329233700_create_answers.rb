class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :token
      t.integer :number
      t.string :answer

      t.timestamps
    end
  end
end
