class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :amount
      t.string :order_type
      t.string :status
      t.integer :user_id

      t.timestamps
    end
  end
end
