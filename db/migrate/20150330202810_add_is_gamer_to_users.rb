class AddIsGamerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_gamer, :boolean
  end
end
