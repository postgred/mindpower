class CreateTurnirs < ActiveRecord::Migration
  def change
    create_table :turnirs do |t|
      t.string :name
      t.string :file_question
      t.string :start_game
      t.string :end_game
      t.text :description

      t.timestamps
    end
  end
end
