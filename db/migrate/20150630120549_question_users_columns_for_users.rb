class QuestionUsersColumnsForUsers < ActiveRecord::Migration
  def self.up
    change_table(:users) do |t|
      t.string :username
      t.string :nickname
      t.string :provider
      t.string :url
      t.string :acc_type
      t.integer :amt
    end
  end

  def self.down
    remove_column :users, :username
    remove_column :users, :nickname
    remove_column :users, :provider
    remove_column :users, :url
  end
end
