class AddJsonToWinpack < ActiveRecord::Migration
  def change
    add_column :winpacks, :json, :text
  end
end
