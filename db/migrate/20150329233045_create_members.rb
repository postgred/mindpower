class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :family
      t.string :name_fst
      t.string :name_scnd
      t.string :membership
      t.string :token

      t.timestamps
    end
  end
end
