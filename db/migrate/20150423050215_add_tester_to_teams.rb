class AddTesterToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :tester, :boolean
  end
end
