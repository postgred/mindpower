class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.integer :user_id
      t.integer :turnir_id

      t.timestamps
    end
    add_index :purchases, :user_id
    add_index :purchases, :turnir_id
    add_index :purchases, [:user_id, :turnir_id], unique: true
  end
end
