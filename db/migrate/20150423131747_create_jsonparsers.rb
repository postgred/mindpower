class CreateJsonparsers < ActiveRecord::Migration
  def change
    create_table :jsonparsers do |t|
      t.text :json_body

      t.timestamps
    end
  end
end
