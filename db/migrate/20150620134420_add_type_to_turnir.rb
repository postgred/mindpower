class AddTypeToTurnir < ActiveRecord::Migration
  def change
    add_column :turnirs, :game_type, :string
  end
end
