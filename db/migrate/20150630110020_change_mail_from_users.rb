class ChangeMailFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :email
    rename_column :users, :mail, :email
  end
end
