class AddExitToToken < ActiveRecord::Migration
  def change
    add_column :tokens, :exit, :boolean
  end
end
