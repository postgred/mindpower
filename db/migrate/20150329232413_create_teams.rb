class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :token
      t.string :team
      t.string :city
      t.integer :turnir_id

      t.timestamps
    end
  end
end
