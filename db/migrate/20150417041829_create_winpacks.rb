class CreateWinpacks < ActiveRecord::Migration
  def change
    create_table :winpacks do |t|
      t.string :platform
      t.float :version
      t.float :minversion
      t.string :file_pack

      t.timestamps
    end
  end
end
