# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150709113220) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: true do |t|
    t.string   "token"
    t.integer  "number"
    t.string   "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "right"
    t.integer  "turnir_id"
    t.integer  "time"
    t.integer  "real_time"
    t.integer  "resulttime"
  end

  create_table "jsonparsers", force: true do |t|
    t.text     "json_body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "logs", force: true do |t|
    t.text     "log"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "members", force: true do |t|
    t.string   "family"
    t.string   "name_fst"
    t.string   "name_scnd"
    t.string   "membership"
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "job_pos"
    t.string   "age"
  end

  create_table "orders", force: true do |t|
    t.integer  "amount"
    t.string   "order_type"
    t.string   "status"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchases", force: true do |t|
    t.integer  "user_id"
    t.integer  "turnir_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "purchases", ["turnir_id"], name: "index_purchases_on_turnir_id", using: :btree
  add_index "purchases", ["user_id", "turnir_id"], name: "index_purchases_on_user_id_and_turnir_id", unique: true, using: :btree
  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "token"
    t.string   "team"
    t.string   "city"
    t.integer  "turnir_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "tester"
  end

  create_table "tokens", force: true do |t|
    t.string   "token"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "exit"
  end

  create_table "turnirs", force: true do |t|
    t.string   "name"
    t.string   "file_question"
    t.string   "start_game"
    t.string   "end_game"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "numbertur"
    t.string   "game_type"
    t.integer  "downtime"
    t.integer  "wrongtime"
    t.integer  "show"
    t.integer  "price",         default: 0
    t.integer  "max_number"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "patronym"
    t.boolean  "is_admin"
    t.boolean  "verifed"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "city"
    t.string   "organisation"
    t.boolean  "is_gamer"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "username"
    t.string   "nickname"
    t.string   "provider"
    t.string   "url"
    t.string   "acc_type"
    t.integer  "amt"
    t.string   "vk"
    t.integer  "check",                  default: 0
    t.string   "fb"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "winpacks", force: true do |t|
    t.string   "platform"
    t.float    "version"
    t.float    "minversion"
    t.string   "file_pack"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "client_type"
    t.text     "json"
  end

end
